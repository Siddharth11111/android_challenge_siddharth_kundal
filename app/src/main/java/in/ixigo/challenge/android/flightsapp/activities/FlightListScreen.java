package in.ixigo.challenge.android.flightsapp.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;

import in.ixigo.challenge.android.flightsapp.R;
import in.ixigo.challenge.android.flightsapp.adapters.FlightsListAdapter;
import in.ixigo.challenge.android.flightsapp.dtos.FlightDTO;
import in.ixigo.challenge.android.flightsapp.network.Requests;
import in.ixigo.challenge.android.flightsapp.network.dtos.FlightResponseDTO;
import in.ixigo.challenge.android.flightsapp.utils.Constants;
import in.ixigo.challenge.android.flightsapp.utils.Util;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class FlightListScreen extends AppCompatActivity implements View.OnClickListener{

    ListView            mFlightsListView;
    FlightsListAdapter  mFlightsAdapter;
    ProgressBar         mFlightsProgressBar;
    RelativeLayout      mSortingOptionsLayout;
    RelativeLayout      mSortOptionDuration;
    RelativeLayout      mSortOptionPrice;

    FlightResponseDTO   mResponseFromServer;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.layout_flights_list_screen);

        initializeViews();

        setOnClickListeners();

        makeRequestToFetchFlightsData();
    }

    private void initializeViews() {
        mFlightsListView        = (ListView)        findViewById(R.id.listFlights);
        mFlightsProgressBar     = (ProgressBar)     findViewById(R.id.flightListProgressBar);
        mSortingOptionsLayout   = (RelativeLayout)  findViewById(R.id.sortOverlayLayout);
        mSortOptionDuration     = (RelativeLayout)  findViewById(R.id.sortOptionDuration);
        mSortOptionPrice        = (RelativeLayout)  findViewById(R.id.sortOptionPrice);
    }

    private void setOnClickListeners() {
        mSortOptionDuration.setOnClickListener(this);
        mSortOptionPrice.setOnClickListener(this);
    }

    private void setUpList(FlightResponseDTO response) {
        Util.log("Setting list up");
        mFlightsAdapter = new FlightsListAdapter(response, getApplicationContext());
        mFlightsListView.setAdapter(mFlightsAdapter);
    }

    private void makeRequestToFetchFlightsData() {

        setUIStateWaiting();

        if(!Util.isNetworkAvailable(getApplicationContext())) {
            Toast.makeText(getApplicationContext(), "Network unavailable. Try later", Toast.LENGTH_SHORT).show();
            setUIStateReady();
            return;
        }

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.FLIGHTS_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        Requests network = retrofit.create(Requests.class);
        final Call<FlightResponseDTO> responseCall = network.getFlightsData();

        try {
            responseCall.enqueue(new Callback<FlightResponseDTO>() {
                @Override
                public void onResponse(Call<FlightResponseDTO> call,
                                       Response<FlightResponseDTO> response) {

                    String status = response.code() + "";
                    Util.log("Status code: " + status);

                    setUIStateReady();

                    FlightResponseDTO responseGot = response.body();

                    mResponseFromServer = responseGot;

                    if (responseGot != null) {

                        ArrayList<FlightDTO> flights = responseGot.getFlights();

                        if(flights != null) {

                            Util.log("Flights size : " + flights.size());

                            if(!flights.isEmpty()) {

                                setUpList(responseGot);

                            } else {

                                Util.log("Flights were empty");

                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<FlightResponseDTO> call, Throwable t) {
                    Util.log("Failure occurred");
                    setUIStateReady();
                }
            });
        } catch (Exception e) {
            Util.log("Exception caught while fetching flights data : " + e.getMessage());
        } finally {
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_listing_screen, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.actionSortList:
                if(mFlightsProgressBar.getVisibility() != View.VISIBLE) {
                    mSortingOptionsLayout.setVisibility(View.VISIBLE);
                } else {
                    Util.log("Loading results so sort option not popping up");
                    Toast.makeText(getApplicationContext(), "Loading results, please wait", Toast.LENGTH_SHORT).show();
                }
                return true;
            case R.id.actionRefresh:
                if(mFlightsProgressBar.getVisibility() != View.VISIBLE) {
                    makeRequestToFetchFlightsData();
                } else {
                    Toast.makeText(getApplicationContext(), "Loading results, please wait", Toast.LENGTH_SHORT).show();
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setUIStateWaiting() {
        mFlightsProgressBar.setVisibility(View.VISIBLE);
    }

    private void setUIStateReady() {
        mFlightsProgressBar.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.sortOptionDuration :
                if(mResponseFromServer != null) {
                    FlightResponseDTO response = mResponseFromServer;
                    Collections.sort(response.getFlights(), new Util.SortBasedOnDuration());
                    setUpList(response);
                }
                mSortingOptionsLayout.setVisibility(View.INVISIBLE);
                break;
            case R.id.sortOptionPrice :
                if(mResponseFromServer != null) {
                    FlightResponseDTO response = mResponseFromServer;
                    Collections.sort(response.getFlights(), new Util.SortBasedOnPrice());
                    setUpList(response);
                }
                mSortingOptionsLayout.setVisibility(View.INVISIBLE);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if(mSortingOptionsLayout.getVisibility() == View.VISIBLE) {
            mSortingOptionsLayout.setVisibility(View.INVISIBLE);
        } else {
            super.onBackPressed();
        }
    }
}
