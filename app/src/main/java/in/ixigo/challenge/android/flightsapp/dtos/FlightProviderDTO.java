package in.ixigo.challenge.android.flightsapp.dtos;

public class FlightProviderDTO {

    String providerId;
    String fare;

    public FlightProviderDTO(String providerId, String fare) {
        this.providerId = providerId;
        this.fare = fare;
    }

    public String getProviderId() {
        return providerId;
    }

    public void setProviderId(String providerId) {
        this.providerId = providerId;
    }

    public String getFare() {
        return fare;
    }

    public void setFare(String fare) {
        this.fare = fare;
    }
}
