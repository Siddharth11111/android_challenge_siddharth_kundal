package in.ixigo.challenge.android.flightsapp.utils;

public class Constants {

    public static final String LOG_TAG = "in.ixigo.challenge.android.flightsapp";
    public static final String FLIGHTS_BASE_URL = "https://firebasestorage.googleapis.com/v0/b/gcm-test-6ab64.appspot.com/o/";
}
