package in.ixigo.challenge.android.flightsapp.network;

import in.ixigo.challenge.android.flightsapp.network.dtos.FlightResponseDTO;
import retrofit2.Call;
import retrofit2.http.GET;

public interface Requests {

    @GET("ixigoandroidchallenge%2Fsample_flight_api_response.json?alt=media&token=d8005801-7878-4f57-a769-ac24133326d6")
    Call<FlightResponseDTO> getFlightsData();
}
