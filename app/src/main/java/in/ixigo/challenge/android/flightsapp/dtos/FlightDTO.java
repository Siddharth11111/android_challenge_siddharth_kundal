package in.ixigo.challenge.android.flightsapp.dtos;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Collections;

import in.ixigo.challenge.android.flightsapp.utils.Util;

public class FlightDTO {

    String originCode;
    String destinationCode;
    String departureTime;
    String arrivalTime;

    ArrayList<FlightProviderDTO> fares;
    String airlineCode;

    @SerializedName("class")
    String ticketClass;

    public FlightDTO(String originCode, String destinationCode, String departureTime,
                     String arrivalTime, ArrayList<FlightProviderDTO> fares, String airlineCode, String ticketClass) {
        this.originCode = originCode;
        this.destinationCode = destinationCode;
        this.departureTime = departureTime;
        this.arrivalTime = arrivalTime;
        this.fares = fares;
        this.airlineCode = airlineCode;
        this.ticketClass = ticketClass;
    }

    public String getOriginCode() {
        return originCode;
    }

    public void setOriginCode(String originCode) {
        this.originCode = originCode;
    }

    public String getDestinationCode() {
        return destinationCode;
    }

    public void setDestinationCode(String destinationCode) {
        this.destinationCode = destinationCode;
    }

    public String getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(String departureTime) {
        this.departureTime = departureTime;
    }

    public String getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(String arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public ArrayList<FlightProviderDTO> getFares() {
        return fares;
    }

    public void setFares(ArrayList<FlightProviderDTO> fares) {
        this.fares = fares;
    }

    public String getAirlineCode() {
        return airlineCode;
    }

    public void setAirlineCode(String airlineCode) {
        this.airlineCode = airlineCode;
    }

    public String getTicketClass() {
        return ticketClass;
    }

    public void setTicketClass(String ticketClass) {
        this.ticketClass = ticketClass;
    }

    public long getDurationDifference() {
        try {
            return Long.parseLong(departureTime) - Long.parseLong(arrivalTime);
        } catch (Exception e) {
            Util.log("Exception parsing arrival and departure times");
            return 99999999;
        }
    }

    public String getLowestPriceString() {
        ArrayList<FlightProviderDTO> flightProviders = fares;
        Collections.sort(fares, new Util.SortProvidersBasedOnPrice());
        return fares.get(0).getFare();
    }
}
