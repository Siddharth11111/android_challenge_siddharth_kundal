package in.ixigo.challenge.android.flightsapp.dtos;

public class FareDTO {

    String providerID;
    String fare;

    public FareDTO(String providerID, String fare) {
        this.providerID = providerID;
        this.fare = fare;
    }

    public String getProviderID() {
        return providerID;
    }

    public void setProviderID(String providerID) {
        this.providerID = providerID;
    }

    public String getFare() {
        return fare;
    }

    public void setFare(String fare) {
        this.fare = fare;
    }
}
