package in.ixigo.challenge.android.flightsapp.network.dtos;

import java.util.ArrayList;

import in.ixigo.challenge.android.flightsapp.dtos.AppendixDTO;
import in.ixigo.challenge.android.flightsapp.dtos.FlightDTO;

public class FlightResponseDTO {

    AppendixDTO appendix;
    ArrayList<FlightDTO> flights;

    public FlightResponseDTO(AppendixDTO appendix, ArrayList<FlightDTO> flights) {
        this.appendix = appendix;
        this.flights = flights;
    }

    public AppendixDTO getAppendix() {
        return appendix;
    }

    public void setAppendix(AppendixDTO appendix) {
        this.appendix = appendix;
    }

    public ArrayList<FlightDTO> getFlights() {
        return flights;
    }

    public void setFlights(ArrayList<FlightDTO> flights) {
        this.flights = flights;
    }
}
