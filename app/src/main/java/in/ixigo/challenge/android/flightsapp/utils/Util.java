package in.ixigo.challenge.android.flightsapp.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;
import java.util.Map;

import in.ixigo.challenge.android.flightsapp.dtos.AppendixDTO;
import in.ixigo.challenge.android.flightsapp.dtos.FlightDTO;
import in.ixigo.challenge.android.flightsapp.dtos.FlightProviderDTO;

public class Util {

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static void log(String whatToLog) {
        Log.wtf(Constants.LOG_TAG, whatToLog);
    }

    /**
     * Method to retrieve the provider name using appendix
     *
     * @param appendix
     * @param providerID
     * @return
     */
    public static String getProviderName(AppendixDTO appendix, String providerID) {
        Map<String, String> providers = appendix.getProviders();
        if(providers.containsKey(providerID)) {
            String providerName = providers.get(providerID);
            return providerName;
        } else {
            return "Unknown Provider";
        }
    }

    /**
     * Method to retrieve airline name using appendix
     *
     * @param appendix
     * @param airlineCode
     * @return
     */
    public static String getAirlineName(AppendixDTO appendix, String airlineCode) {
        Map<String, String> airlines = appendix.getAirlines();
        if(airlines.containsKey(airlineCode)) {
            String providerName = airlines.get(airlineCode);
            return providerName;
        } else {
            return "Unknown Airline";
        }
    }

    /**
     * Method to convert time to correct format to display
     *
     * @param epochTimeString
     * @return
     */
    public static String getTimeInCorrectFormat(String epochTimeString) {
        long epochTime = Long.parseLong(epochTimeString);

        try {
            Date date = new Date(epochTime);
//            DateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            DateFormat format = new SimpleDateFormat("HH:mm");
//        format.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
            String formattedTime = format.format(date);
            return formattedTime;

        } catch (Exception e) {
            Util.log("Exception parsing time");
            return "Unknown time";
        }
    }

    /**
     * Method to get duration of flight
     *
     * @param epochDeparture
     * @param epochArrival
     * @return
     */
    public static String getFlightDuration(String epochDeparture, String epochArrival) {
        try {
            long depTime = Long.parseLong(epochDeparture);
            long arrivalTime = Long.parseLong(epochArrival);
            long duration = (arrivalTime - depTime) / (60 * 60 * 1000);
            Util.log("duration : " + duration);
            return duration + " hours";
        } catch (Exception e) {
            Util.log("Exception parsing duration");
            return "Duration unknown";
        }
    }

    /**
     * Comparator to sort the list of flights based on duration
     *
     */
    public static class SortBasedOnDuration implements Comparator {

        public int compare(Object o1, Object o2) {
            long dd1 = ((FlightDTO) o1).getDurationDifference();
            long dd2 = ((FlightDTO) o2).getDurationDifference();
            if(dd1 > dd2) {
                return -1;
            } else if(dd1 < dd2) {
                return 1;
            } else {
                return 0;
            }
        }
    }

    /**
     * Comparator to sort the list of prices based on price
     *
     */
    public static class SortProvidersBasedOnPrice implements Comparator {

        public int compare(Object o1, Object o2) {
            long dd1 = Long.parseLong(((FlightProviderDTO) o1).getFare());
            long dd2 = Long.parseLong(((FlightProviderDTO) o2).getFare());
            if(dd1 > dd2) {
                return 1;
            } else if(dd1 < dd2) {
                return -1;
            } else {
                return 0;
            }
        }
    }

    /**
     * Comparator to sort the list of flights based on price
     *
     */
    public static class SortBasedOnPrice implements Comparator {

        public int compare(Object o1, Object o2) {
            long dd1 = Long.parseLong(((FlightDTO) o1).getLowestPriceString());
            long dd2 = Long.parseLong(((FlightDTO) o2).getLowestPriceString());
            if(dd1 > dd2) {
                return 1;
            } else if(dd1 < dd2) {
                return -1;
            } else {
                return 0;
            }
        }
    }
}
