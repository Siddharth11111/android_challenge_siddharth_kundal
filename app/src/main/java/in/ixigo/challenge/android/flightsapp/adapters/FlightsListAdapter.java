package in.ixigo.challenge.android.flightsapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;

import in.ixigo.challenge.android.flightsapp.R;
import in.ixigo.challenge.android.flightsapp.dtos.FlightDTO;
import in.ixigo.challenge.android.flightsapp.dtos.FlightProviderDTO;
import in.ixigo.challenge.android.flightsapp.network.dtos.FlightResponseDTO;
import in.ixigo.challenge.android.flightsapp.utils.Util;

public class FlightsListAdapter extends BaseAdapter{

    FlightResponseDTO       mFlightResponse;
    ArrayList<FlightDTO>    mFlights;
    Context                 mContext;

    public FlightsListAdapter(FlightResponseDTO flightResponse, Context context) {
        mFlightResponse = flightResponse;
        mFlights        = flightResponse.getFlights();
        mContext        = context;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        FlightItemViewHolder holder;

        if(convertView == null) {

            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_flight_list, null);

            holder = new FlightItemViewHolder();

            holder.flightAirlineView        = (TextView)        convertView.findViewById(R.id.flightItemAirline);
            holder.flightClassView          = (TextView)        convertView.findViewById(R.id.flightItemClass);
            holder.flightTimingsView        = (TextView)        convertView.findViewById(R.id.flightItemTiming);
            holder.flightDurationView       = (TextView)        convertView.findViewById(R.id.flightItemDuration);
            holder.flightProvidersLayout    = (LinearLayout)    convertView.findViewById(R.id.layoutProviders);
            convertView.setTag(holder);

        } else {

            holder = (FlightItemViewHolder) convertView.getTag();

        }

        FlightDTO flight = mFlights.get(i);
        holder.flightAirlineView.setText(Util.getAirlineName(mFlightResponse.getAppendix(),
                flight.getAirlineCode()));

        holder.flightClassView.setText(flight.getTicketClass());
        holder.flightTimingsView.setText(Util.getTimeInCorrectFormat(flight.getDepartureTime())
                + " -> " + Util.getTimeInCorrectFormat(flight.getArrivalTime()));
        holder.flightDurationView.setText(Util.getFlightDuration(flight.getDepartureTime(), flight.getArrivalTime()));

        ArrayList<FlightProviderDTO> flightProviders = flight.getFares();
        Collections.sort(flightProviders, new Util.SortProvidersBasedOnPrice());

        if(flightProviders!= null && !flightProviders.isEmpty()) {

            holder.flightProvidersLayout.removeAllViews();

            for(FlightProviderDTO provider : flightProviders) {
                View viewToAdd = LayoutInflater.from(mContext).inflate(R.layout.item_flight_provider, null);
                TextView flightFareView       = (TextView) viewToAdd.findViewById(R.id.flightItemFare);
                TextView flightProviderView   = (TextView) viewToAdd.findViewById(R.id.flightItemProvider);

                flightProviderView.setText(Util.getProviderName(mFlightResponse.getAppendix(),
                        provider.getProviderId()));

                flightFareView.setText(mContext.getResources().getString(R.string.rupee_icon) + " " + provider.getFare());

                holder.flightProvidersLayout.addView(viewToAdd);
            }
        }
        return convertView;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public Object getItem(int i) {
        return mFlights.get(i);
    }

    @Override
    public int getCount() {
        return mFlights.size();
    }

    class FlightItemViewHolder {

        TextView        flightAirlineView;
        TextView        flightClassView;
        TextView        flightTimingsView;
        TextView        flightDurationView;
        LinearLayout    flightProvidersLayout;
    }
}