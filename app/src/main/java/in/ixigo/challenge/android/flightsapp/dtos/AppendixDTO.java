package in.ixigo.challenge.android.flightsapp.dtos;

import java.util.Map;

public class AppendixDTO {

    Map<String, String> airlines;
    Map<String, String> airports;
    Map<String, String> providers;

    public AppendixDTO(Map<String, String> airlines, Map<String, String> airports, Map<String, String> providers) {
        this.airlines = airlines;
        this.airports = airports;
        this.providers = providers;
    }

    public Map<String, String> getAirlines() {
        return airlines;
    }

    public void setAirlines(Map<String, String> airlines) {
        this.airlines = airlines;
    }

    public Map<String, String> getAirports() {
        return airports;
    }

    public void setAirports(Map<String, String> airports) {
        this.airports = airports;
    }

    public Map<String, String> getProviders() {
        return providers;
    }

    public void setProviders(Map<String, String> providers) {
        this.providers = providers;
    }
}
